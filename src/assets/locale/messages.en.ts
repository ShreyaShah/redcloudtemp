export const Translation = `
<?xml version="1.0" encoding="UTF-8" ?>
<xliff version="1.2" xmlns="urn:oasis:names:tc:xliff:document:1.2">
  <file source-language="en" datatype="plaintext" original="ng2.template">
    <body>
      <trans-unit id="25afb65a599c1d4107b1b60c928283ff8a31ba19" datatype="html">
        <source><x id="START_ITALIC_TEXT" ctype="x-i"/>RED<x id="CLOSE_ITALIC_TEXT" ctype="x-i"/>.cloud
                        </source>
        <target/>
      </trans-unit>
      <trans-unit id="bb694b49d408265c91c62799c2b3a7e3151c824d" datatype="html">
        <source>Logout</source>
        <target>Logout</target>
      </trans-unit>
      <trans-unit id="f147d0f7f965cccee2e77294cba8e1b88021fa08" datatype="html">
        <source>Upgrade</source>
        <target>Upgrade</target>
      </trans-unit>
    </body>
  </file>
</xliff>
`;

