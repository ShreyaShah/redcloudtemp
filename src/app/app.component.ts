import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Constant } from './common/constant';
import { Common } from './common/common';
import { ToasterService } from './services/toaster.service';
import { Message } from 'primeng/primeng';
import { Idle, DEFAULT_INTERRUPTSOURCES } from '@ng-idle/core';
import { AuthService } from './login/authentication.service';
import { ErrorService } from './errorhandler/error-handler.service';

@Component({
    selector: 'app-emagine',
    templateUrl: './app.component.html',
    styleUrls: ['./app.component.css']
})

export class AppComponent implements OnInit {
    subscription: any;
    constructor(
        private common: Common,
        private _router: Router,
        private constant: Constant,
        private toasterService: ToasterService,
        private idle: Idle,
        private authService: AuthService,
        private errorService: ErrorService) {
    }

    msgs: Message[];
    invalidImageDisplay: boolean = false;
    invalidImageFile: boolean = false;
    ngOnInit(): void {
        //opens the pop up when the image size is not valid
        this.subscription = this.common.getEmittedValueImg()
            .subscribe(item => this.invalidImageDisplay = item);
        //opens the popup when the uploaded file type is not valid
        this.subscription = this.common.getEmittedValueValidImg()
            .subscribe(item => this.invalidImageFile = item);
        this.subscription = this.toasterService.getToaster().subscribe(message => {
            this.msgs = message;
        });
        if (this.common.isLoggedIn()) {
            // sets an idle timeout of 1800 seconds.
            this.idle.setIdle(this.constant.idleTimeout);
            // sets a timeout period of 1800 seconds. after another 1800
            // seconds of inactivity which is  warning period,
            // user would be logged out..
            this.idle.setTimeout(this.constant.idleTimeout);
            this.idle.setInterrupts(DEFAULT_INTERRUPTSOURCES);
            // logout after timeout and  redirect to login
            let ctrl = this;
            this.idle.onTimeout.subscribe(() => {
                ctrl.authService.logout(true);
                ctrl.authService.goToErrorPageDueToSessionTimeout();
            });
            //Starts the idle timer to watch when the user is idle
            this.idle.watch();
        }

    }
    invalidImageDisplayFun() {
        this.invalidImageDisplay = false;
        this.invalidImageFile = false;
    }
    isHeaderRequired() {
        if (this._router.url === this.constant.errorUrl
            || this._router.url === this.constant.forgotPasswordUrl
            || this._router.url === this.constant.resetPasswordUrl) {
            return false;
        }
        return true;
    }

    isSidemenuHumburgerRequired() {
        if (this._router.url === this.constant.dashboardUrl || this._router.url == '/') {
            return false;
        }
        return true;
    }

    changeState(id, e, caratId) {
        //On click of ul toggle the open/close dropdown
        $("#" + id).next('ul').toggle();
        if ($("#" + id).next('ul').is(':visible')) {
            //Get the first child of the ul and change the caret symbol from open to close and vice versa
            $("#" + caratId).addClass("menu-open-caret");
            $("#" + caratId).removeClass("menu-close-caret");
        } else {
            $("#" + caratId).addClass("menu-close-caret");
            $("#" + caratId).removeClass("menu-open-caret");
        }
    }

    closeMenus() {
        $(".sidemenuOpenLeftUl li a").next('ul').hide();
        $(".caratBtn").addClass("menu-close-caret");
        $(".caratBtn").removeClass("menu-open-caret");
    }
}
