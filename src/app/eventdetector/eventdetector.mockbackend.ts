import {BaseRequestOptions, Response, ResponseOptions, RequestMethod,Http} from '@angular/http';
import {MockBackend, MockConnection} from '@angular/http/testing';
import {EventDetectorMockDataService} from '../eventdetector/eventdetector.mockdata.service';
import {HttpService} from '../services/http.service';

export let EventDetectorMockBackendProvider = {
    // use fake backend in place of Http service for backend-less development
    provide: HttpService,
    useFactory: (backend: MockBackend, options: BaseRequestOptions) => {
        // configure fake backend
        backend.connections.subscribe((connection: MockConnection) => {
            
            let eventDetectorMockDataService = new EventDetectorMockDataService();
            
            if (connection.request.url.endsWith('ered/page/eventdetectors?pageIndex=0&pageSize=2000') && connection.request.method === RequestMethod.Get) {
                let eventDetectors = eventDetectorMockDataService.getAllEventDetector();
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: eventDetectors})
                ));
            }
            if (connection.request.url.endsWith('ered/eventdetectors') && connection.request.method === RequestMethod.Post) {
                let newEventDetector = JSON.parse(connection.request.getBody());
                newEventDetector.eventDetectorId = 7;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: newEventDetector})
                ));
            }
            if (connection.request.url.endsWith('ered/eventdetectors/') && connection.request.method === RequestMethod.Put) {
                let newEventDetector = JSON.parse(connection.request.getBody());
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: newEventDetector})
                ));
            }
            if (connection.request.url.endsWith('ered/publish/eventdetectors/') && connection.request.method === RequestMethod.Post) {
                let newEventDetector = JSON.parse(connection.request.getBody());
                newEventDetector.status = "Published";
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200, body: newEventDetector})
                ));
            }
            if (connection.request.url.includes('ered/eventdetectors/') && connection.request.method === RequestMethod.Delete) {
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200})
                ));
            }
            if (connection.request.url.includes('ered/archive/eventdetectors/') && connection.request.method === RequestMethod.Put) {
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200})
                ));
            }
            if (connection.request.url.includes('ered/version/eventdetectors/') && connection.request.method === RequestMethod.Put) {
                let edList = eventDetectorMockDataService.getAllEventDetector();
                edList[edList.length - 1].version = edList[edList.length - 1].version + 0.1;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200,body:edList[edList.length - 1]})
                ));
            }
            if (connection.request.url.includes('ered/copy/eventdetectors/') && connection.request.method === RequestMethod.Put) {
                let edList = eventDetectorMockDataService.getAllEventDetector();
                edList[edList.length - 1].eventName = "Duplicate of Event Detector - " + edList[edList.length - 1].eventName;
                connection.mockRespond(new Response(
                    new ResponseOptions({status: 200,body:edList[edList.length - 1]})
                ));
            }
        });

        return new Http(backend, options);
    },
    deps: [MockBackend, BaseRequestOptions]
};