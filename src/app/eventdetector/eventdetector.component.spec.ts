/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { BaseRequestOptions } from '@angular/http';
import { MockBackend } from '@angular/http/testing';
import { BrowserModule } from '@angular/platform-browser';
import { NoopAnimationsModule, BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { APP_BASE_HREF } from '@angular/common';
import { EventDetectorComponent } from './eventdetector.component';
import { SharedModule } from '../shared/shared.module';
import { EventDetectorService } from './eventdetector.service';
import { EventDetectorMockBackendProvider } from './eventdetector.mockbackend';
import { SuppressionService } from '../suppression/suppression.service';
import { FeedsService } from '../feeds/feeds.service';
import { ConsumerService } from '../consumer/consumer.service';
import { CustomerContextRecordsService } from '../customercontextrecords/customer-context-records.service';
import { Configuration } from '../services/configuration';
import { EventDetectorMockDataService } from './eventdetector.mockdata.service';
import { Common } from '../common/common';
import { Constant } from '../common/constant';
import * as _ from 'lodash';

describe('EventDetectorComponent', () => {
    let eventDetectorCompInstance: EventDetectorComponent;
    let fixture: ComponentFixture<EventDetectorComponent>;
    let eventDetectorMockDataService = new EventDetectorMockDataService();
     beforeAll(function() {
        jasmine.DEFAULT_TIMEOUT_INTERVAL = 999999;
    });
    beforeEach(async(() => {
        TestBed.configureTestingModule({
            imports: [
                RouterTestingModule,
                NoopAnimationsModule,
                BrowserAnimationsModule,
                SharedModule,
                BrowserModule
            ],
            declarations: [
                EventDetectorComponent
            ],
            providers: [
                EventDetectorService,
                SuppressionService,
                FeedsService,
                ConsumerService,
                CustomerContextRecordsService,
                Configuration,
                MockBackend,
                EventDetectorMockBackendProvider,
                BaseRequestOptions,
                Common,
                Constant,
                { provide: APP_BASE_HREF, useValue: '/' }
            ]
        }).compileComponents();

    }));

    //rcw:comment Shreya | -To check whether the component exists or not
    it('should create the event detector comp', async(() => {
        console.log("Create Event Detector Component");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        expect(eventDetectorCompInstance).toBeTruthy();
    }));

    //rcw:comment Shreya | -To check the click of the tab and select the tab wise event detector display
    it('Set Tab Parameter', async(() => {
        console.log("Set Tab Parameter In Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        eventDetectorCompInstance.setTabParameters('config', 2);
        expect(eventDetectorCompInstance.selectedTab).toEqual('config');
    }));

    //rcw:comment Shreya | -Compute the rule expression based on any given rule json 
    it('Computing Rule Expression', async(() => {
        console.log("Computing Rule Expression In Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        let str = eventDetectorCompInstance.computed({ "operator": "AND", "rules": [{ "condition": "=", "field": "Temp Name - -1524061161", "data": "36" }, { "condition": "=", "field": "supp-update", "data": "96" }] });
        expect(str).toEqual('(Temp Name - -1524061161 = 36 <strong>AND</strong> supp-update = 96)');
    }));

    //rcw:comment Shreya | -Add Event Detector creating the event detector object from mockbackend
    // and calling the add function
    // from the component.
    it('Add Event Detector', function (done) {
        console.log("Add Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            eventDetectorCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = eventDetectorCompInstance.configurationEventDetectorList.length;
            let eventDetector = eventDetectorMockDataService.createNewEventDetector();
            eventDetectorCompInstance.eventDetectorObj = eventDetector;
            eventDetectorCompInstance.addOrUpdateEventDetector();
            setTimeout(function () {
                fixture.detectChanges();
                expect(eventDetectorCompInstance.configurationEventDetectorList.length).toEqual(originalLength + 1);
                done();
            }, 500);
        });

    });

    //rcw:comment Shreya | -Update Event Detector taking event detector object from the list updating the same
    // and calling the update function
    // from the component.
    it('Update Event Detector', function (done) {
        console.log("Update Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            eventDetectorCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            eventDetectorCompInstance.selectedIndex = eventDetectorCompInstance.configurationEventDetectorList.length - 1;
            eventDetectorCompInstance.eventDetectorObj = _.cloneDeep(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1]);
            eventDetectorCompInstance.eventDetectorObj = _.cloneDeep(eventDetectorMockDataService.updateEventDetector(eventDetectorCompInstance.eventDetectorObj));
            eventDetectorCompInstance.addOrUpdateEventDetector();
            setTimeout(function () {
                fixture.detectChanges();
                expect(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1].eventName).toEqual(eventDetectorCompInstance.eventDetectorObj.eventName);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | -Publishing Event Detector sending the draft status event detector
    // and changing the status from "DRAFT" to "PUBLISH"
    // from the component.
    it('Publish Event Detector', function (done) {
        console.log("Publish Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            eventDetectorCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            eventDetectorCompInstance.selectedIndex = eventDetectorCompInstance.configurationEventDetectorList.length - 1;
            eventDetectorCompInstance.eventDetectorObj = _.cloneDeep(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1]);
            eventDetectorCompInstance.publishEventDetectors();
            setTimeout(function () {
                fixture.detectChanges();
                expect(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1].eventName).toEqual(eventDetectorCompInstance.eventDetectorObj.eventName);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | -Delete Event Detector calling delete method from component
    it('Delete Event Detector', function (done) {
        console.log("Delete Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            eventDetectorCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = eventDetectorCompInstance.configurationEventDetectorList.length;
            eventDetectorCompInstance.modalContent = _.cloneDeep(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1]);
            eventDetectorCompInstance.deleteEventDetector();
            setTimeout(function () {
                fixture.detectChanges();
                expect(eventDetectorCompInstance.configurationEventDetectorList.length).toEqual(originalLength - 1);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | -Archive Event Detector calling archive method from component
    it('Archive Event Detector', function (done) {
        console.log("Archive Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            eventDetectorCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            const originalLength = eventDetectorCompInstance.configurationEventDetectorList.length;
            eventDetectorCompInstance.modalContent = _.cloneDeep(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1]);
            eventDetectorCompInstance.archiveEventDetector();
            setTimeout(function () {
                fixture.detectChanges();
                expect(eventDetectorCompInstance.configurationEventDetectorList.length).toEqual(originalLength - 1);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | -Generating Next Version of selected Event Detector.
    it('Next Version of Event Detector', function (done) {
        console.log("Next Version of Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            eventDetectorCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            let eventDetector = _.cloneDeep(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1]);
            eventDetectorCompInstance.generateNextVersion(eventDetector);
            setTimeout(function () {
                fixture.detectChanges();
                expect(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1].version).toEqual(eventDetector.version + 0.1);
                done();
            }, 500);
        });
    });

    //rcw:comment Shreya | -Duplicating the selected Event Detector.
    it('Duplicate Event Detector', function (done) {
        console.log("Duplicate Event Detector");
        fixture = TestBed.createComponent(EventDetectorComponent);
        eventDetectorCompInstance = fixture.debugElement.componentInstance;
        fixture.detectChanges();
        fixture.whenStable().then(() => {
            eventDetectorCompInstance.setTabParameters('config', 2);
            fixture.detectChanges();
            let eventDetector = _.cloneDeep(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1]);
            eventDetectorCompInstance.duplicateEventDetector(eventDetector);
            setTimeout(function () {
                fixture.detectChanges();
                expect(eventDetectorCompInstance.configurationEventDetectorList[eventDetectorCompInstance.configurationEventDetectorList.length - 1].eventName).toEqual("Duplicate of Event Detector - " + eventDetector.eventName);
                done();
            }, 500);
        });
    });


});