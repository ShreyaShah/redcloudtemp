/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {HeaderComponent} from './header.component';
import {HeaderService} from './header.service';
import {SharedModule} from '../shared/shared.module';
import {AppRoutingModule} from '../app-routing.module';
import {ConfirmDialogModule} from 'primeng/primeng';
@NgModule({
    imports: [
        CommonModule,
        FormsModule,
        SharedModule,
        AppRoutingModule,
        ConfirmDialogModule
    ],
    declarations: [
        HeaderComponent
    ],
    exports:[
         HeaderComponent
    ],
    providers: [HeaderService]
})
export class HeaderModule {

}

