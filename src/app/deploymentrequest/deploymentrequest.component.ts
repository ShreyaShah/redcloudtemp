/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import { Component, OnInit, ViewChild, AfterViewInit, ElementRef } from '@angular/core';
import { Router } from '@angular/router';
import { DeploymentRequestService } from './deploymentrequest.service';
import { DeploymentRequest } from './deploymentrequest';
import { UsecaseService } from '../usecases/usecases.service';
import { UseCase } from '../usecases/usecases';
import { EventDetector } from '../eventdetector/eventdetector';
import { EventDetectorService } from '../eventdetector/eventdetector.service';
import { DataTable } from 'primeng/components/datatable/datatable';
import * as _ from "lodash";

@Component({
    moduleId: module.id,
    selector: 'deployment-request',
    templateUrl: 'deploymentrequest.component.html',
})
export class DeploymentRequestComponent implements OnInit, AfterViewInit {

    @ViewChild('usecaseTbl')
    usecaseTbl: DataTable;

    @ViewChild('gbUsecase') search1: ElementRef;

    @ViewChild('deploymentRequestForm') deploymentRequestForm: any;

    constructor(
        private deploymentRequestService: DeploymentRequestService,
        private usecaseService: UsecaseService,
        private eventDetectorService: EventDetectorService,
        private router: Router
    ) { };

    useCases: UseCase[] = [];
    eventDetectors: EventDetector[] = [];
    workflows: any[] = [];
    deploymentList: any = [];
    selectedUseCases: UseCase[] = [];
    selectedEventDetectors: EventDetector[] = [];
    selectedWorkflows: any[] = [];
    libPriorityList: any[] = [];
    eventDetectorPriorityList: EventDetector[] = [];
    usecasePriorityList: UseCase[] = [];
    workflowPriorityList: any[] = [];
    originalEventDetectorPriorityList: EventDetector[] = [];
    originalUsecasePriorityList: UseCase[] = [];
    originalWorkflowPriorityList: any[] = [];

    selectedTab: any = 1;
    mainSelectedTab: any = 0;

    deploymentRequestObj = new DeploymentRequest();
    days = Array.from(Array(31), (e, i) => i);
    months = ["Jan", "Feb", "Mar", "Apr", "May", "Jun", "Jul", "Aug", "Sep", "Oct", "Nov", "Dec"];
    years = [];
    submittedFlag: boolean = false;
    isValidDeployment: boolean = false;
    deploymentReqs: DeploymentRequest[] = [];
    showOnlyChanges: boolean = true;
    priorityConfirmDisplay: boolean = false;
    warningDisplay: boolean = false;

    libraryList = [
        { name: 'Event Detectors', value: 'Event Detectors', checked: false },
        { name: 'Workflows', value: 'Workflows', checked: false },
        { name: 'Usecases', value: 'Usecases', checked: false }
    ];
    tabs = [
        { id: 1, title: "1. Deploy to Production", active: true },
        { id: 2, title: "2. View Changes", active: false },
        { id: 3, title: "3. Prioritise", active: false }
    ];
    isRequesting: boolean = false;
    mainTabs = [
        { id: 1, title: "Deployment Environment", active: true },
        { id: 2, title: "Deployable Libraries", active: false },
        { id: 3, title: "Event Detectors", active: false },
        { id: 2, title: "Workflows", active: false },
        { id: 2, title: "Usecases", active: false },
        { id: 2, title: "Schedule Deployment", active: false },
        { id: 2, title: "Approval", active: false },
    ];
    selectedEnv: string = '';
    libraryFlag = {}
    deploymentLibList: any = [];
    usecaseSearch: string = '';

    ngAfterViewInit() {
    }

    ngOnInit(): void {
        var d = new Date();
        //Considered the year from current to next 10 yrs
        for (let i = 2016, max = d.getFullYear() + 10; i <= max; i++) {
            this.years.push(i);
        }
        this.getAllUseCases();
        this.getAllEventDetectors();
    }

    // rcw:comment Shreya | To generate label based on the libraries selected and on the traversal of last tab of that particular current libraries
    generateLastLabel() {
        //If selected main tab is Event Detectors then based on the other two libraries selected generate the label
        if (this.mainSelectedTab == 2) {
            //If Workflow library is checked then the next label for the button is WORKFLOW
            if (this.libraryList[1].checked) {
                return "Workflows";
                //If Usecase library is checked then the next label for the button is USECASE
            } else if (this.libraryList[2].checked) {
                return "Use Cases";
            } else {
                //If no other library other than event detector is selected then directly go to schedule deployment
                return "Schedule Deployment";
            }
        } else if (this.mainSelectedTab == 3) {
            if (this.libraryList[2].checked) {
                return "Use Cases";
            } else {
                return "Schedule Deployment";
            }
        } else if (this.mainSelectedTab == 4) {
            return "Schdule Deployment";
        }
    }

    // rcw:comment Shreya | The main navigation method to go through the library and deployment tabs
    selectMainNav(currentTabId: any, noCompare: any) {
        let rtEdsMatched: boolean = true;
        //If current selected tab is selection of libraries
        if (currentTabId == 1) {
            //First deselect all the previosly selected main tabs
            this.deMainSelectTab();
            //Then de-select all the previously selected nav tabs of library
            this.deselectTab();
            //Then pre-select the first tab of Event detector library
            this.tabs[0].active = true;
            this.selectedTab = this.tabs[0].id;
            this.mainTabs[currentTabId + 1].active = true;
            this.mainSelectedTab = currentTabId + 1;
            this.deploymentLibList = _.cloneDeep(this.eventDetectors);
        } else if (currentTabId == 2) {
            //check whether to compare of not compare the priority list
            //If to be compared then compare the priority list with the oiriginal list
            //If the order of both list is same then generate warning message else go forward
            if (!noCompare) {
                rtEdsMatched = this.compareEd();
            }
            //If changed then go forward
            if (rtEdsMatched) {
                this.warningDisplay = false;
                this.deMainSelectTab();
                this.deselectTab();
                this.tabs[0].active = true;
                this.selectedTab = this.tabs[0].id;
                //Save the priority into the appropriate list
                this.eventDetectorPriorityList = _.cloneDeep(this.libPriorityList);
                //Go to next library main tabs
                if (this.libraryList[1].checked) {
                    this.mainSelectedTab = 3;
                    this.mainTabs[3].active = true;
                    this.deploymentLibList = [];
                } else if (this.libraryList[2].checked) {
                    this.mainSelectedTab = 4;
                    this.mainTabs[4].active = true;
                    this.deploymentLibList = _.cloneDeep(this.useCases);
                } else {
                    this.mainSelectedTab = 5;
                    this.mainTabs[5].active = true;
                }
            } else {
                this.openPriorityConfirmation();
            }

        } else if (currentTabId == 3) {
            if (!noCompare) {
                rtEdsMatched = this.compareEd();
            }
            if (rtEdsMatched) {
                this.warningDisplay = false;
                this.deMainSelectTab();
                this.deselectTab();
                this.tabs[0].active = true;
                this.selectedTab = this.tabs[0].id;
                this.workflowPriorityList = _.cloneDeep(this.libPriorityList);
                if (this.libraryList[2].checked) {
                    this.mainSelectedTab = 4;
                    this.mainTabs[4].active = true;
                    this.deploymentLibList = _.cloneDeep(this.useCases);
                } else {
                    this.mainSelectedTab = 5;
                    this.mainTabs[5].active = true;
                }
            } else {
                this.openPriorityConfirmation();
            }

        } else if (currentTabId == 4) {
            if (!noCompare) {
                rtEdsMatched = this.compareEd();
            }
            if (rtEdsMatched) {
                this.warningDisplay = false;
                this.deMainSelectTab();
                this.deselectTab();
                this.tabs[0].active = true;
                this.selectedTab = this.tabs[0].id;
                this.usecasePriorityList = _.cloneDeep(this.libPriorityList);
                this.mainSelectedTab = 5;
                this.mainTabs[5].active = true;
            } else {
                this.openPriorityConfirmation();
            }
        } else {
            this.deMainSelectTab();
            this.deselectTab();
            this.tabs[0].active = true;
            this.selectedTab = this.tabs[0].id;
            this.mainTabs[currentTabId + 1].active = true;
            this.mainSelectedTab = currentTabId + 1;
        }
        window.scrollTo(0, 0)
    }

    // rcw:comment Shreya | Set the checked libraries into the appropriate list
    setSelected(data: any) {
        //If we are on Event Detector main tabs
        if (this.mainSelectedTab == 2) {
            //If the event Detector is selected then push into the seperate list to flow that list everywhere
            if (data.isSelected) {
                //Add selected event detectors to the list
                this.selectedEventDetectors.push(data);
                //For combining the production and selected event detector for the priority tab
                this.eventDetectorPriorityList.push(data);
                //If the event Detector is selected then add that ed from the original prority list
                this.originalEventDetectorPriorityList.push(data);
                //To set isSelected property into the main list as we have used a common list for all libraries on UI.
                let index = _.findIndex(this.eventDetectors, function (o) { return o.eventDetectionId == data.eventDetectionId; });
                //If found then replace the data.
                if (index > -1) {
                    this.eventDetectors[index] = data;
                }
            } else {
                //If the event Detector is un-selected then remove that ed from the seperate list
                let index = _.findIndex(this.selectedEventDetectors, function (o) { return o.eventDetectionId == data.eventDetectionId; });
                if (index > -1) {
                    this.selectedEventDetectors.splice(index, 1);
                }
                //If the event Detector is un-selected then remove that ed from the prority list
                index = _.findIndex(this.eventDetectorPriorityList, function (o) { return o.eventDetectionId == data.eventDetectionId; });
                if (index > -1) {
                    this.eventDetectorPriorityList.splice(index, 1);
                }
                //If the event Detector is un-selected then remove that ed from the original prority list
                index = _.findIndex(this.originalEventDetectorPriorityList, function (o) { return o.eventDetectionId == data.eventDetectionId; });
                if (index > -1) {
                    this.originalEventDetectorPriorityList.splice(index, 1);
                }
                //Update the value of is selected property into the original list as well to maintain consistency as we have used common list on UI
                index = _.findIndex(this.eventDetectors, function (o) { return o.eventDetectionId == data.eventDetectionId; });
                //If found then replace the data.
                if (index > -1) {
                    this.eventDetectors[index] = data;
                }
            }
            //If we are on use case main tabs
        } else if (this.mainSelectedTab == 4) {
            //If the Use Case is selected then push into the seperate list to flow that list everywhere
            if (data.isSelected) {
                //Add selected Use Case to the list
                this.selectedUseCases.push(data);
                //For combining the production and selected Use Case for the priority tab
                this.usecasePriorityList.push(data);
                //For combining the production and selected Use Case for the original priority tab
                this.originalUsecasePriorityList.push(data);
                //To set isSelected property into the main list as we have used a common list for all libraries on UI.
                let index = _.findIndex(this.useCases, function (o) { return o.useCaseId == data.useCaseId; });
                //If found then replace the data.
                if (index > -1) {
                    this.useCases[index] = data;
                }
            } else {
                //If the Use Case is un-selected then remove that usecase from the seperate list
                let index = _.findIndex(this.selectedUseCases, function (o) { return o.useCaseId == data.useCaseId; });
                if (index > -1) {
                    this.selectedUseCases.splice(index, 1);
                }
                //If the Use Case is un-selected then remove that usecase from the original prority list
                index = _.findIndex(this.usecasePriorityList, function (o) { return o.useCaseId == data.useCaseId; });
                if (index > -1) {
                    this.usecasePriorityList.splice(index, 1);
                }
                //If the Use Case is un-selected then remove that usecase from the  prority list
                index = _.findIndex(this.originalUsecasePriorityList, function (o) { return o.useCaseId == data.useCaseId; });
                if (index > -1) {
                    this.originalUsecasePriorityList.splice(index, 1);
                }
                //Update the value of is selected property into the original list as well to maintain consistency as we have used common list on UI
                index = _.findIndex(this.useCases, function (o) { return o.useCaseId == data.useCaseId; });
                //If found then replace the data.
                if (index > -1) {
                    this.useCases[index] = data;
                }
            }
        }
    }

    // rcw:comment Shreya | Change the dropdown arrow
    changeState(id, e) {
        //On click of ul toggle the open/close dropdown
        $("#" + id).next('ul').toggle();
        if ($("#" + id).next('ul').is(':visible')) {
            //Get the first child of the ul and change the caret symbol from open to close and vice versa
            $("#" + id + " i:first-child").addClass("deploy-open-caret");
            $("#" + id + " i:first-child").removeClass("deploy-close-caret");
        } else {
            $("#" + id + " i:first-child").addClass("deploy-close-caret");
            $("#" + id + " i:first-child").removeClass("deploy-open-caret");
        }
        e.stopPropagation();
        e.preventDefault();
    }

    // rcw:comment Shreya |  - Get All the Event detectors which are of selected status from the server
    getAllEventDetectors() {
        this.isRequesting = true;
        var ctrl = this;
        this.eventDetectorService.getPaged(0, 2000)
            .then(function (eds) {
                for (let item of eds) {
                    item.isSelected = false;
                }
                ctrl.eventDetectors = _.cloneDeep(eds);
            }).then(function () {
                ctrl.isRequesting = false;
            }).catch(function () {
                ctrl.isRequesting = false;
            });
    }

    // rcw:comment Shreya | Clear the search on changing the tabs
    clearSearch() {
        this.usecaseSearch = '';
        let event = new KeyboardEvent('keyup');
        this.search1.nativeElement.dispatchEvent(event);
    }

    // rcw:comment Shreya | Back navigation of main tabs
    goToBackMainNav(currentTabId: number) {
        this.deMainSelectTab();
        this.deselectTab();
        this.tabs[2].active = true;
        this.selectedTab = this.tabs[2].id;
        if (currentTabId == 1 || currentTabId == 2) {
            this.mainTabs[this.mainSelectedTab - 1].active = true;
            this.mainSelectedTab = this.mainSelectedTab - 1;
        } else if (currentTabId == 3) {
            if (this.libraryList[0].checked) {
                this.mainSelectedTab = 2;
                this.mainTabs[2].active = true;
                this.deploymentLibList = _.cloneDeep(this.eventDetectors);
            }
        } else if (currentTabId == 4) {
            if (this.libraryList[1].checked) {
                this.mainSelectedTab = 3;
                this.mainTabs[3].active = true;
                this.deploymentLibList = [];
            } else if (this.libraryList[0].checked) {
                this.mainSelectedTab = 2;
                this.mainTabs[2].active = true;
                this.deploymentLibList = _.cloneDeep(this.eventDetectors);
            }
        } else if (currentTabId == 5) {
            if (this.libraryList[2].checked) {
                this.mainSelectedTab = 4;
                this.mainTabs[4].active = true;
                this.deploymentLibList = _.cloneDeep(this.useCases);
            } else if (this.libraryList[1].checked) {
                this.mainSelectedTab = 3;
                this.mainTabs[3].active = true;
                this.deploymentLibList = [];
            } else if (this.libraryList[0].checked) {
                this.mainSelectedTab = 2;
                this.mainTabs[2].active = true;
                this.deploymentLibList = _.cloneDeep(this.eventDetectors);
            }
        } else {
            this.mainTabs[this.mainSelectedTab - 1].active = true;
            this.mainSelectedTab = this.mainSelectedTab - 1;
        }
        window.scrollTo(0, 0)
    }

    // rcw:comment Shreya | Navigation into the inner tabs of the selected libraries
    selectNav(tab, $event, deploymentRequestForm) {
        if (tab.id == 4) {
            var rtEdsMatched = this.compareEd();
            if (rtEdsMatched) {
                this.deselectTab();
                tab.active = true;
                this.selectedTab = tab.id;
            } else {
                $event.stopPropagation();
                $event.preventDefault();
            }
        } else if (tab.id == 3) {
            if (this.mainSelectedTab == 2) {
                this.libPriorityList = _.cloneDeep(this.eventDetectorPriorityList);
            } else if (this.mainSelectedTab == 3) {

            } else if (this.mainSelectedTab == 4) {
                this.libPriorityList = [];
                this.libPriorityList = _.cloneDeep(this.usecasePriorityList);
            }
            this.deselectTab();
            tab.active = true;
            this.selectedTab = tab.id;
        } else if (tab.id == 5) {
            if (deploymentRequestForm.form.valid) {
                this.deselectTab();
                tab.active = true;
                this.selectedTab = tab.id;
            } else {
                $event.stopPropagation();
                $event.preventDefault();
            }
        } else {
            this.deselectTab();
            tab.active = true;
            this.selectedTab = tab.id;
        }
        window.scrollTo(0, 0)
    }

    // rcw:comment Shreya | Validate process on click of validate button and check for UI validation as well
    validateDeployment(deploymentRequestForm) {
        this.submittedFlag = true;
        if (deploymentRequestForm.form.valid) {
            this.submittedFlag = false;
            this.isValidDeployment = true;
        }
    }

    // rcw:comment Shreya | Get All the Usecases from the database having the status as published
    getAllUseCases() {
        let ctrl = this;
        this.usecaseService.get(0, 2000)
            .then(function (usecase) {
                ctrl.useCases = _.cloneDeep(usecase);
            });
    }

    // rcw:comment Shreya | Get All the other pending deployment request for the last tab
    getAllPendingDeploymentReq() {
        this.deploymentReqs = [];
        let dep = new DeploymentRequest();
        dep.deploymentRequestId = 1;
        dep.packageId = "PROD_976";
        dep.deploymentRequestName = "Name 1";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 2;
        dep.packageId = "PROD_1156";
        dep.deploymentRequestName = "Name 2";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 3;
        dep.packageId = "PROD_1267";
        dep.deploymentRequestName = "Name 3";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 4;
        dep.packageId = "PROD_1298";
        dep.deploymentRequestName = "Name 4";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 5;
        dep.packageId = "PROD_1235";
        dep.deploymentRequestName = "Name 5";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Awaiting Approval";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 6;
        dep.packageId = "PROD_1641";
        dep.deploymentRequestName = "Name 6";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Requested Deployment";
        this.deploymentReqs.push(dep);
        dep = new DeploymentRequest();
        dep.deploymentRequestId = 7;
        dep.packageId = "PROD_1642";
        dep.deploymentRequestName = "Name 7";
        dep.scheduledFor = "10/02/17 at 02:30 pm";
        dep.scheduledBy = "Greg Armstrong";
        dep.scheduledOn = "01/02/17 at 11:48 am";
        dep.status = "Requested Deployment";
        this.deploymentReqs.push(dep);
    }

    // rcw:comment Shreya | Common method to first de-select all the previously selected inner nav tabs into libraries
    deselectTab() {
        this.tabs.forEach((nav) => {
            nav.active = false;
        });
    }

    // rcw:comment Shreya | Common method to first de-select all the previously selected main tabs
    deMainSelectTab() {
        this.mainTabs.forEach((nav) => {
            nav.active = false;
        });
    }

    // rcw:comment Shreya | Go to inner view changes tabs from the listed libraries
    goToViewChanges() {
        this.deselectTab();
        this.tabs[1].active = true;
        this.selectedTab = 2;
        window.scrollTo(0, 0)
    }

    // rcw:comment Shreya | Go to priority tab from the view changes and 
    // if any previously priority has been set then effect those changes
    goToPriorities() {
        this.deselectTab();
        this.tabs[2].active = true;
        this.selectedTab = 3;
        if (this.mainSelectedTab == 2) {
            this.libPriorityList = _.cloneDeep(this.eventDetectorPriorityList);
        } else if (this.mainSelectedTab == 3) {
            this.libPriorityList = [];
        } else if (this.mainSelectedTab == 4) {
            this.libPriorityList = _.cloneDeep(this.usecasePriorityList);
        }
        window.scrollTo(0, 0)
    }

    // rcw:comment Shreya | Go to schedule deployment after selecting all the libraries needed to be deployed
    goToScheduleDeployment() {
        var rtEdsMatched = this.compareEd();
        if (rtEdsMatched) {
            this.deselectTab();
            this.deMainSelectTab();
            this.mainTabs[5].active = true;
            this.mainSelectedTab = 5;
        } else {
            this.openPriorityConfirmation();
        }
        window.scrollTo(0, 0)
    }

    // rcw:comment Shreya | Check for validations from database and if any exist show warning message
    goToApproval() {
        this.warningDisplay = true;
    }

    // rcw:comment Shreya | After successfull validations generate all the pending deployment
    goToDone() {
        this.submittedFlag = true;
        if (this.deploymentRequestForm.form.valid) {
            this.submittedFlag = false;
            this.deselectTab();
            this.deMainSelectTab();
            this.mainTabs[6].active = true;
            this.mainSelectedTab = 6;
            this.getAllPendingDeploymentReq();
        }
        window.scrollTo(0, 0)
    }

    // rcw:comment Shreya |Click to done after request approval
    onDone() {
        console.log("done");
    }

    // rcw:comment Shreya | Go back traversal inside the inner tabs
    goToBack() {
        this.deselectTab();
        let tab = this.selectedTab - 2;
        this.tabs[tab].active = true;
        this.selectedTab = this.selectedTab - 1;
        this.isValidDeployment = false;
        window.scrollTo(0, 0)
    }

    // rcw:comment Shreya | Compare the libraries based on the current selected tabs
    compareEd() {
        if (this.mainSelectedTab == 2) {
            for (var i = 0; i < this.originalEventDetectorPriorityList.length; i++) {
                // Search every object in the job.data array for a match.
                // If found return false to remove this object from the results
                if (this.libPriorityList[i].eventName !== this.originalEventDetectorPriorityList[i].eventName) {
                    return true;
                }
            }
        } else if (this.mainSelectedTab == 3) {
            for (var i = 0; i < this.originalWorkflowPriorityList.length; i++) {
                // Search every object in the job.data array for a match.
                // If found return false to remove this object from the results
                if (this.libPriorityList[i].workflowName !== this.originalWorkflowPriorityList[i].workflowName) {
                    return true;
                }
            }
        } else if (this.mainSelectedTab == 4) {
            for (var i = 0; i < this.originalUsecasePriorityList.length; i++) {
                // Search every object in the job.data array for a match.
                // If found return false to remove this object from the results
                if (this.libPriorityList[i].useCaseName !== this.libPriorityList[i].useCaseName) {
                    return true;
                }
            }
        }
        return false;
    }

    // rcw:comment Shreya | Confirmation popup 
    openPriorityConfirmation() {
        this.priorityConfirmDisplay = true;
    }

    // rcw:comment Shreya | Whenever user wants to reject the
    // deployment request remove the dep req from the list
    cancelPendingApproval(pendingReq, index) {
        this.deploymentReqs.splice(index, 1);
    }

}

