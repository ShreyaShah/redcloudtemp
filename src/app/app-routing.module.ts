import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';

import {DashboardComponent} from './dashboard/dashboard.component';
import {LoginComponent} from './login/login.component';
import {EventDetectorComponent} from './eventdetector/eventdetector.component';
import {SuppressionComponent} from './suppression/suppression.component';
import {UsecaseComponent} from './usecases/usecases.component';
import {UserManagementComponent} from './usermanagement/usermanagement.component';
import {DeploymentRequestComponent} from './deploymentrequest/deploymentrequest.component';
import {DeploymentComponent} from './deploymentrequest/deployment.component';
import {PendingApprovalComponent} from './deploymentrequest/pendingapproval.component';
import {ActionsComponent} from './actions/actions.component';
import {SegmentComponent} from './segments/segment.component';
import {AuthGuard} from './guards/auth.guard';
import {ResetPasswordGuard} from './guards/resetpassword.guard';
import {ForgotPasswordComponent} from './forgotpassword/forgotpassword.component';
import {ResetPasswordComponent} from './resetpassword/resetpassword.component';
import {ErrorHandlerComponent} from './errorhandler/error-handler.component';
import {ChangePasswordComponent} from './changepassword//changepassword.component';
import {ComingSoonComponent} from './comingsoon/comingsoon.component';
import {ExplorationComponent} from './exploration/exploration.component';
import {MonitoringComponent} from './monitoring/monitoring.component';

const routes: Routes = [

  {path: 'login', component: LoginComponent},
  /*    { path: '', component: DashboardComponent, canActivate: [AuthGuard] },*/
  {path: '', redirectTo: '/dashboard', pathMatch: 'full',canActivate: [AuthGuard]},
  {path: 'dashboard', component: DashboardComponent,canActivate: [AuthGuard]},
  {path: 'eventdetectors', component: EventDetectorComponent, canDeactivate: [AuthGuard],canActivate: [AuthGuard]},
  {path: 'suppressions', component: SuppressionComponent, canDeactivate: [AuthGuard],canActivate: [AuthGuard]},
  {path: 'usecases', component: UsecaseComponent, canDeactivate: [AuthGuard],canActivate: [AuthGuard]},
  {path: 'deploymentrequest', component: DeploymentRequestComponent,canActivate: [AuthGuard]},
  {path: 'deployment', component: DeploymentComponent,canActivate: [AuthGuard]},
  {path: 'pendingapproval', component: PendingApprovalComponent,canActivate: [AuthGuard]},
  {path: 'actions', component: ActionsComponent,canActivate: [AuthGuard]},
  {path: 'segments', component: SegmentComponent, canDeactivate: [AuthGuard],canActivate: [AuthGuard]},
  {path: 'users', component: UserManagementComponent, canDeactivate: [AuthGuard],canActivate: [AuthGuard]},
  {path: 'forgotpassword', component: ForgotPasswordComponent,canActivate: [AuthGuard]},
  {path: 'resetpassword', component: ResetPasswordComponent , canActivate:[ResetPasswordGuard]},
  {path: 'changepassword', component: ChangePasswordComponent , canActivate:[AuthGuard]},
  {path: 'error', component: ErrorHandlerComponent },
  {path: 'comingsoon/:featureName',component: ComingSoonComponent },
  {path: 'bdna/:featureName',component: ExplorationComponent,canActivate: [AuthGuard]},
  {path: 'dropoffoffers/:featureName',component: ExplorationComponent,canActivate: [AuthGuard]},
  {path: 'dropoffsimoffers/:featureName',component: ExplorationComponent,canActivate: [AuthGuard]},
  {path: 'campaignmetrics/:featureName',component: ExplorationComponent,canActivate: [AuthGuard]},
  {path: 'performance/:featureName',component: ExplorationComponent,canActivate: [AuthGuard]},
  {path: 'overview/:featureName',component: MonitoringComponent,canActivate: [AuthGuard]},
  {path: 'charts/:featureName',component: MonitoringComponent,canActivate: [AuthGuard]},
  {path: 'environments/:featureName',component: MonitoringComponent,canActivate: [AuthGuard]},
  {path: 'alerts/:featureName',component: MonitoringComponent,canActivate: [AuthGuard]},
  {path: '**', redirectTo: '/dashboard'}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule {
}

export const routedComponents = [DashboardComponent];
