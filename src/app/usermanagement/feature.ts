/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

export class Feature {

    public screenId: number;
    public screenName: string;
    public pageAccess: boolean;
    public updateAccess: boolean;
    public roleId:number;

    constructor() {
        this.pageAccess = true;
        this.updateAccess = true;
    }
}
