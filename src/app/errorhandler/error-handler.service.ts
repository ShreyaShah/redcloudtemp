import { Injectable } from "@angular/core";
import { Constant } from '../common/constant';

@Injectable()
export class ErrorService {

    public content: String;
    public header : String;
    public buttonName : String;
    public buttonLink : String

    constructor(private constant:Constant) { 
      this.content="No error currently";
      this.header="Error";
      this.buttonName="Go To Dashboard";
      this.buttonLink=this.constant.dashboardUrl;
    }
}