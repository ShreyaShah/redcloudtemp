import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ActivatedRoute, Params } from '@angular/router';
import { ErrorService } from './error-handler.service';
import { Constant } from '../common/constant';

@Component({
    moduleId: module.id,
    templateUrl: 'error-handler.component.html'

})

export class ErrorHandlerComponent implements OnInit {

    constructor(private route: ActivatedRoute,
        private router: Router,
        private errorService: ErrorService,
        private activatedRoute: ActivatedRoute,
        private constant: Constant) {
    }

    public errorContent: String;
    public errorHeader: String;
    public errorButtonName: String
    public errorButtonLink: String;

    ngOnInit() {

        this.activatedRoute.queryParams.subscribe(params => {
            let actionValue = params['action'];
            if (actionValue == "sessionTimeout") {
                this.errorContent = "You need to login again!";
                this.errorHeader = "Session Expired";
                this.errorButtonName = "Go to Login";
                this.errorButtonLink = this.constant.loginUrl;
            }
            else {
                this.errorContent = this.errorService.content;
                this.errorHeader = this.errorService.header;
                this.errorButtonName = this.errorService.buttonName;
                this.errorButtonLink = this.errorService.buttonLink;
            }
        });
    }


    onClickButton() {
        this.router.navigate([this.errorButtonLink]);
    }


}