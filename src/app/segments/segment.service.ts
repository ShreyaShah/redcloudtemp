/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import {Injectable} from '@angular/core';
import 'rxjs/add/operator/toPromise';
import {Segments} from '../segments/segment';
import {Configuration} from '../services/configuration';
import {HttpService} from '../services/http.service';
import {Common} from '../common/common';

@Injectable()
export class SegmentService {
    
    private actionUrl: string;
    constructor(private http:HttpService, private configuration: Configuration,private common:Common) {
        this.actionUrl = configuration.ServerWithApiUrl;
    }
    
    get(): Promise<Segments[]> {
        return this.http
            .get(this.actionUrl +'ered/segment')
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    getById(id: number): Promise<Segments> {
        return this.http.get(this.actionUrl +"ered/segment/" + id)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }

    add(segment: Segments) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/segment", segment)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    update(suppression: Segments) : Promise<any> {
        return this.http.put(this.actionUrl +"ered/segment", suppression)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    publish(segment: Segments) : Promise<any> {
        return this.http.post(this.actionUrl +"ered/publish/segment/", segment)
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    copy(id: number): Promise<any> {
        console.log("id :"+id)
        return this.http.put(this.actionUrl +"ered/copy/segment/"+id,"")
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
    
    archive(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/archive/segment/"+id,"")
            .toPromise()
            .then(response => response)
            .catch(this.common.handleError);
    }
    
    delete(id: number): Promise<any> {
        return this.http.delete(this.actionUrl +"ered/segment/"+id)
            .toPromise()
            .then(response => response)
            .catch(this.common.handleError);
    }
    
    nextVersion(id: number): Promise<any> {
        return this.http.put(this.actionUrl +"ered/version/segment/"+id, "")
            .toPromise()
            .then(response => response.json())
            .catch(this.common.handleError);
    }
}
