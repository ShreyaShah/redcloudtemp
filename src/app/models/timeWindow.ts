export class TimeWindow {
    public id: number;
    public eventDetectionId: number;
    public startTime: string;
    public startHrTime: string;
    public startMinTime: string;
    public startMeridian: string;
    public endTime: string;
    public endHrTime: string;
    public endMinTime: string;
    public endMeridian: string;
    public timeWindowDay: string;
    constructor() {
        this.startMeridian = "am";
        this.endMeridian = "pm";
    };
}
